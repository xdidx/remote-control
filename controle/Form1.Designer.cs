﻿namespace DiDConnect
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label6 = new System.Windows.Forms.Label();
            this.PB_reduct = new System.Windows.Forms.PictureBox();
            this.PB_close = new System.Windows.Forms.PictureBox();
            this.onglets = new DiDConnect.TabControl();
            this.onglet_accueil = new System.Windows.Forms.TabPage();
            this.PB_remoteScreen = new System.Windows.Forms.PictureBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.L_remoteName = new System.Windows.Forms.Label();
            this.TB_remotePassword = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TB_remoteID = new System.Windows.Forms.NumericUpDown();
            this.B_connexion = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.L_remoteCPUUsage = new System.Windows.Forms.Label();
            this.L_remoteRAMUsage = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CPUUsage = new System.Windows.Forms.Label();
            this.RAMUsage = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.L_computerPassword = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.L_computerID = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.L_statut = new System.Windows.Forms.Label();
            this.onglet_options = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.CB_proxy = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TB_password = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TB_passwordConfirm = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.B_changePassword = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TB_pseudo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.B_changeName = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PB_reduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_close)).BeginInit();
            this.onglets.SuspendLayout();
            this.onglet_accueil.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PB_remoteScreen)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TB_remoteID)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.onglet_options.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(250, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 18);
            this.label6.TabIndex = 21;
            this.label6.Text = "DiD Connect";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.label6.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.label6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // PB_reduct
            // 
            this.PB_reduct.Image = global::DiDConnect.Properties.Resources.reduire;
            this.PB_reduct.InitialImage = ((System.Drawing.Image)(resources.GetObject("PB_reduct.InitialImage")));
            this.PB_reduct.Location = new System.Drawing.Point(547, 0);
            this.PB_reduct.Name = "PB_reduct";
            this.PB_reduct.Size = new System.Drawing.Size(28, 28);
            this.PB_reduct.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.PB_reduct.TabIndex = 20;
            this.PB_reduct.TabStop = false;
            this.PB_reduct.Click += new System.EventHandler(this.PB_reduct_Click);
            this.PB_reduct.MouseEnter += new System.EventHandler(this.PB_reduct_MouseEnter);
            this.PB_reduct.MouseLeave += new System.EventHandler(this.PB_reduct_MouseLeave);
            // 
            // PB_close
            // 
            this.PB_close.Image = global::DiDConnect.Properties.Resources.fermer;
            this.PB_close.InitialImage = ((System.Drawing.Image)(resources.GetObject("PB_close.InitialImage")));
            this.PB_close.Location = new System.Drawing.Point(572, 0);
            this.PB_close.Name = "PB_close";
            this.PB_close.Size = new System.Drawing.Size(28, 28);
            this.PB_close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.PB_close.TabIndex = 19;
            this.PB_close.TabStop = false;
            this.PB_close.Click += new System.EventHandler(this.PB_close_Click);
            this.PB_close.MouseEnter += new System.EventHandler(this.PB_close_MouseEnter);
            this.PB_close.MouseLeave += new System.EventHandler(this.PB_close_MouseLeave);
            // 
            // onglets
            // 
            this.onglets.Controls.Add(this.onglet_accueil);
            this.onglets.Controls.Add(this.onglet_options);
            this.onglets.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.onglets.Location = new System.Drawing.Point(0, 27);
            this.onglets.Name = "onglets";
            this.onglets.Padding = new System.Drawing.Point(0, 0);
            this.onglets.SelectedIndex = 0;
            this.onglets.Size = new System.Drawing.Size(600, 273);
            this.onglets.TabIndex = 18;
            // 
            // onglet_accueil
            // 
            this.onglet_accueil.BackColor = System.Drawing.Color.Transparent;
            this.onglet_accueil.Controls.Add(this.PB_remoteScreen);
            this.onglet_accueil.Controls.Add(this.groupBox5);
            this.onglet_accueil.Controls.Add(this.groupBox4);
            this.onglet_accueil.Controls.Add(this.groupBox3);
            this.onglet_accueil.Location = new System.Drawing.Point(4, 25);
            this.onglet_accueil.Name = "onglet_accueil";
            this.onglet_accueil.Padding = new System.Windows.Forms.Padding(3);
            this.onglet_accueil.Size = new System.Drawing.Size(592, 244);
            this.onglet_accueil.TabIndex = 0;
            this.onglet_accueil.Text = "Accueil";
            // 
            // PB_remoteScreen
            // 
            this.PB_remoteScreen.Location = new System.Drawing.Point(298, 115);
            this.PB_remoteScreen.Name = "PB_remoteScreen";
            this.PB_remoteScreen.Size = new System.Drawing.Size(286, 123);
            this.PB_remoteScreen.TabIndex = 14;
            this.PB_remoteScreen.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.L_remoteName);
            this.groupBox5.Controls.Add(this.TB_remotePassword);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.TB_remoteID);
            this.groupBox5.Controls.Add(this.B_connexion);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.L_remoteCPUUsage);
            this.groupBox5.Controls.Add(this.L_remoteRAMUsage);
            this.groupBox5.Location = new System.Drawing.Point(6, 112);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.groupBox5.Size = new System.Drawing.Size(286, 126);
            this.groupBox5.TabIndex = 13;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Informations de l\'ordinateur distant";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 40);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "Nom :";
            // 
            // L_remoteName
            // 
            this.L_remoteName.AutoSize = true;
            this.L_remoteName.Location = new System.Drawing.Point(49, 40);
            this.L_remoteName.Name = "L_remoteName";
            this.L_remoteName.Size = new System.Drawing.Size(16, 13);
            this.L_remoteName.TabIndex = 18;
            this.L_remoteName.Text = " - ";
            // 
            // TB_remotePassword
            // 
            this.TB_remotePassword.Location = new System.Drawing.Point(180, 17);
            this.TB_remotePassword.Name = "TB_remotePassword";
            this.TB_remotePassword.PasswordChar = '*';
            this.TB_remotePassword.Size = new System.Drawing.Size(100, 20);
            this.TB_remotePassword.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(137, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "MDP :";
            // 
            // TB_remoteID
            // 
            this.TB_remoteID.Location = new System.Drawing.Point(52, 17);
            this.TB_remoteID.Name = "TB_remoteID";
            this.TB_remoteID.Size = new System.Drawing.Size(67, 20);
            this.TB_remoteID.TabIndex = 14;
            // 
            // B_connexion
            // 
            this.B_connexion.Enabled = false;
            this.B_connexion.Location = new System.Drawing.Point(6, 97);
            this.B_connexion.Name = "B_connexion";
            this.B_connexion.Size = new System.Drawing.Size(274, 23);
            this.B_connexion.TabIndex = 13;
            this.B_connexion.Text = "Connexion";
            this.B_connexion.UseVisualStyleBackColor = true;
            this.B_connexion.Click += new System.EventHandler(this.B_connexion_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(24, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "ID :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "CPU :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 80);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "RAM :";
            // 
            // L_remoteCPUUsage
            // 
            this.L_remoteCPUUsage.AutoSize = true;
            this.L_remoteCPUUsage.Location = new System.Drawing.Point(49, 60);
            this.L_remoteCPUUsage.Name = "L_remoteCPUUsage";
            this.L_remoteCPUUsage.Size = new System.Drawing.Size(16, 13);
            this.L_remoteCPUUsage.TabIndex = 9;
            this.L_remoteCPUUsage.Text = " - ";
            // 
            // L_remoteRAMUsage
            // 
            this.L_remoteRAMUsage.AutoSize = true;
            this.L_remoteRAMUsage.Location = new System.Drawing.Point(49, 80);
            this.L_remoteRAMUsage.Name = "L_remoteRAMUsage";
            this.L_remoteRAMUsage.Size = new System.Drawing.Size(16, 13);
            this.L_remoteRAMUsage.TabIndex = 10;
            this.L_remoteRAMUsage.Text = " - ";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.CPUUsage);
            this.groupBox4.Controls.Add(this.RAMUsage);
            this.groupBox4.Location = new System.Drawing.Point(298, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.groupBox4.Size = new System.Drawing.Size(286, 100);
            this.groupBox4.TabIndex = 12;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Informations de l\'ordinateur";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "CPU :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "RAM :";
            // 
            // CPUUsage
            // 
            this.CPUUsage.AutoSize = true;
            this.CPUUsage.Location = new System.Drawing.Point(52, 18);
            this.CPUUsage.Name = "CPUUsage";
            this.CPUUsage.Size = new System.Drawing.Size(117, 13);
            this.CPUUsage.TabIndex = 9;
            this.CPUUsage.Text = "Chargement en cours...";
            // 
            // RAMUsage
            // 
            this.RAMUsage.AutoSize = true;
            this.RAMUsage.Location = new System.Drawing.Point(52, 40);
            this.RAMUsage.Name = "RAMUsage";
            this.RAMUsage.Size = new System.Drawing.Size(117, 13);
            this.RAMUsage.TabIndex = 10;
            this.RAMUsage.Text = "Chargement en cours...";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox3.Controls.Add(this.L_computerPassword);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.L_computerID);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.L_statut);
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.groupBox3.Size = new System.Drawing.Size(286, 100);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Informations de connexion";
            // 
            // L_computerPassword
            // 
            this.L_computerPassword.AutoSize = true;
            this.L_computerPassword.Location = new System.Drawing.Point(125, 60);
            this.L_computerPassword.Name = "L_computerPassword";
            this.L_computerPassword.Size = new System.Drawing.Size(110, 13);
            this.L_computerPassword.TabIndex = 10;
            this.L_computerPassword.Text = "Connexion en cours...";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 60);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "Mot de passe : ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 40);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(96, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "ID de l\'ordinateur : ";
            // 
            // L_computerID
            // 
            this.L_computerID.AutoSize = true;
            this.L_computerID.Location = new System.Drawing.Point(125, 40);
            this.L_computerID.Name = "L_computerID";
            this.L_computerID.Size = new System.Drawing.Size(110, 13);
            this.L_computerID.TabIndex = 8;
            this.L_computerID.Text = "Connexion en cours...";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Statut de l\'ordinateur : ";
            // 
            // L_statut
            // 
            this.L_statut.AutoSize = true;
            this.L_statut.Location = new System.Drawing.Point(125, 20);
            this.L_statut.Name = "L_statut";
            this.L_statut.Size = new System.Drawing.Size(110, 13);
            this.L_statut.TabIndex = 6;
            this.L_statut.Text = "Connexion en cours...";
            // 
            // onglet_options
            // 
            this.onglet_options.BackColor = System.Drawing.Color.Transparent;
            this.onglet_options.Controls.Add(this.groupBox6);
            this.onglet_options.Controls.Add(this.groupBox1);
            this.onglet_options.Controls.Add(this.groupBox2);
            this.onglet_options.Location = new System.Drawing.Point(4, 25);
            this.onglet_options.Name = "onglet_options";
            this.onglet_options.Padding = new System.Windows.Forms.Padding(3);
            this.onglet_options.Size = new System.Drawing.Size(592, 244);
            this.onglet_options.TabIndex = 2;
            this.onglet_options.Text = "Options";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox6.Controls.Add(this.CB_proxy);
            this.groupBox6.Location = new System.Drawing.Point(7, 113);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(285, 100);
            this.groupBox6.TabIndex = 18;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Proxy";
            // 
            // CB_proxy
            // 
            this.CB_proxy.AutoSize = true;
            this.CB_proxy.Location = new System.Drawing.Point(8, 19);
            this.CB_proxy.Name = "CB_proxy";
            this.CB_proxy.Size = new System.Drawing.Size(100, 17);
            this.CB_proxy.TabIndex = 0;
            this.CB_proxy.Text = "Utiliser un proxy";
            this.CB_proxy.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox1.Controls.Add(this.TB_password);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.TB_passwordConfirm);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.B_changePassword);
            this.groupBox1.Location = new System.Drawing.Point(298, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.groupBox1.Size = new System.Drawing.Size(286, 100);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Changer le mot de passe de l\'ordinateur";
            // 
            // TB_password
            // 
            this.TB_password.Location = new System.Drawing.Point(118, 15);
            this.TB_password.Name = "TB_password";
            this.TB_password.PasswordChar = '*';
            this.TB_password.Size = new System.Drawing.Size(162, 20);
            this.TB_password.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Confirmation";
            // 
            // TB_passwordConfirm
            // 
            this.TB_passwordConfirm.Location = new System.Drawing.Point(118, 41);
            this.TB_passwordConfirm.Name = "TB_passwordConfirm";
            this.TB_passwordConfirm.PasswordChar = '*';
            this.TB_passwordConfirm.Size = new System.Drawing.Size(162, 20);
            this.TB_passwordConfirm.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Mot de passe";
            // 
            // B_changePassword
            // 
            this.B_changePassword.Enabled = false;
            this.B_changePassword.Location = new System.Drawing.Point(6, 67);
            this.B_changePassword.Name = "B_changePassword";
            this.B_changePassword.Size = new System.Drawing.Size(274, 23);
            this.B_changePassword.TabIndex = 13;
            this.B_changePassword.Text = "Changer le mot de passe";
            this.B_changePassword.UseVisualStyleBackColor = true;
            this.B_changePassword.Click += new System.EventHandler(this.B_changePassword_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox2.Controls.Add(this.TB_pseudo);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.B_changeName);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.groupBox2.Size = new System.Drawing.Size(286, 100);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Changer de nom";
            // 
            // TB_pseudo
            // 
            this.TB_pseudo.Location = new System.Drawing.Point(95, 15);
            this.TB_pseudo.Name = "TB_pseudo";
            this.TB_pseudo.Size = new System.Drawing.Size(185, 20);
            this.TB_pseudo.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 18);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Nom du PC";
            // 
            // B_changeName
            // 
            this.B_changeName.Enabled = false;
            this.B_changeName.Location = new System.Drawing.Point(6, 41);
            this.B_changeName.Name = "B_changeName";
            this.B_changeName.Size = new System.Drawing.Size(274, 23);
            this.B_changeName.TabIndex = 13;
            this.B_changeName.Text = "Changer de pseudo";
            this.B_changeName.UseVisualStyleBackColor = true;
            this.B_changeName.Click += new System.EventHandler(this.B_changeName_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(600, 300);
            this.Controls.Add(this.PB_reduct);
            this.Controls.Add(this.onglets);
            this.Controls.Add(this.PB_close);
            this.Controls.Add(this.label6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "Form1";
            this.TransparencyKey = System.Drawing.Color.Gray;
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.PB_reduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_close)).EndInit();
            this.onglets.ResumeLayout(false);
            this.onglet_accueil.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PB_remoteScreen)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TB_remoteID)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.onglet_options.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label L_statut;
        public System.Windows.Forms.Label CPUUsage;
        public System.Windows.Forms.Label RAMUsage;
        public System.Windows.Forms.Label L_computerID;
        public System.Windows.Forms.Label L_remoteCPUUsage;
        public System.Windows.Forms.Label L_remoteRAMUsage;
        public System.Windows.Forms.CheckBox CB_proxy;
        public System.Windows.Forms.TextBox TB_pseudo;
        public System.Windows.Forms.Button B_changePassword;
        public System.Windows.Forms.Button B_changeName;
        public System.Windows.Forms.Button B_connexion;
        public System.Windows.Forms.TextBox TB_password;
        public System.Windows.Forms.TextBox TB_passwordConfirm;
        public System.Windows.Forms.PictureBox PB_remoteScreen;

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private TabControl onglets;
        private System.Windows.Forms.TabPage onglet_accueil;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TabPage onglet_options;
        private System.Windows.Forms.PictureBox PB_close;
        private System.Windows.Forms.PictureBox PB_reduct;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown TB_remoteID;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox TB_remotePassword;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label L_remoteName;
        public System.Windows.Forms.Label L_computerPassword;
        private System.Windows.Forms.Label label14;
    }
}

