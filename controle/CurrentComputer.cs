﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Microsoft.VisualBasic;
using System.Collections.Specialized;
using System.IO;
using System.Windows.Forms;
using System.Net;
using System.Drawing;

namespace DiDConnect
{
    class CurrentComputer
    {
        private System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

        private string filePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\DiDConnect\\";
        private string fileName = "information.did";
        private bool connectedComputer = false;
        private string computerID = "-";        

        static private Chiffrement chiffrement = new Chiffrement();

        private PerformanceCounter cpuCounter;
        private PerformanceCounter ramCounter;
        private Form1 fenetre;
        private WebConnexion webConnexion;

        public CurrentComputer(Form1 newFenetre)
        {
            fenetre = newFenetre;
            webConnexion = new WebConnexion(fenetre);

            cpuCounter = new PerformanceCounter();
            ramCounter = new PerformanceCounter("Memory", "Available MBytes");
            cpuCounter.CategoryName = "Processor";
            cpuCounter.CounterName = "% Processor Time";
            cpuCounter.InstanceName = "_Total";
            
        }

        private void majDonnees(object sender, EventArgs e)
        {

            if (connectedComputer)
            {
                //maj données ordinateur
                fenetre.CPUUsage.Text = getCurrentCpuUsage();
                fenetre.RAMUsage.Text = getAvailableRAM() + " / " + getMaxRAM();

                NameValueCollection POSTData = new NameValueCollection();
                
                
                POSTData["image"] = getStringFromScreen();
                POSTData["computerID"] = computerID;
                webConnexion.request(WebConnexion.POSTOperations.majScreen, POSTData, true);

                POSTData = new NameValueCollection();
                 

                POSTData["CPUUsage"] = fenetre.CPUUsage.Text;
                POSTData["RAMUsage"] = fenetre.RAMUsage.Text;
                POSTData["computerID"] = computerID;

                Dictionary<string, string> reponse = webConnexion.request(WebConnexion.POSTOperations.majDonnees, POSTData);
                if (reponse["state"] != "1")
                {
                    connectedComputer = false;
                    MessageBox.Show(reponse["state"] + " et " + reponse["message"]);
                }

            }
            else
            {
                verificationComputerId();
            }
            updateInterface();
        }

        private void updateInterface()
        {
            if (connectedComputer)
            {
                fenetre.L_statut.Text = "Ordinateur connecté au serveur";
                fenetre.L_computerID.Text = computerID;
                fenetre.B_changeName.Enabled = true;
                fenetre.B_changePassword.Enabled = true;
                fenetre.B_connexion.Enabled = true;
            }
            else
            {
                fenetre.L_statut.Text = "Problèmes de connexion...";
                fenetre.L_computerID.Text = "-";
                fenetre.B_changeName.Enabled = false;
                fenetre.B_changePassword.Enabled = false;
                fenetre.B_connexion.Enabled = false;
            }
        }

        public bool connexion()
        {
            if(webConnexion.verifConnexion())
                if(!verificationComputerId())
                    addComputer();

            if (connectedComputer)
            {
                timer.Tick += new EventHandler(majDonnees);
                timer.Interval = 500;//ms
                timer.Enabled = true;
                timer.Start();
            }
            return connectedComputer;
        }

        private bool verificationComputerId()
        {
            if (File.Exists(Path.Combine(filePath, fileName)))
            {
                string[] lines = File.ReadAllLines(Path.Combine(filePath, fileName));
                if (lines.Length > 0)
                {
                    NameValueCollection POSTData = new NameValueCollection();
                    try
                    {
                        POSTData["computerID"] = chiffrement.dechiffrer(lines[0]);

                        Dictionary<string, string> reponse = webConnexion.request(WebConnexion.POSTOperations.verifID, POSTData);
                        if (reponse["state"] == "1")
                        {
                            computerID = reponse["computerID"];
                            fenetre.TB_pseudo.Text = reponse["computerName"];
                            fenetre.TB_password.Text = reponse["computerFixPassword"];
                            fenetre.TB_passwordConfirm.Text = reponse["computerFixPassword"];
                            fenetre.L_computerPassword.Text = reponse["computerPassword"];
                            
                            connectedComputer = true;
                            updateInterface();
                            return true;
                        }
                    }
                    catch (Exception) 
                    {
                        MessageBox.Show("Fichier de configuration invalide");
                    }
                }
            }
            return false;
        }

        private bool addComputer()
        {
            NameValueCollection POSTData = new NameValueCollection();
            POSTData["RAM"] = getMaxRAM();

            Dictionary<string, string> reponse = webConnexion.request(WebConnexion.POSTOperations.newComputer, POSTData);
            if (reponse["state"] == "1")
            {
                try
                {
                    int test = Convert.ToInt32(reponse["newID"]);

                    Directory.CreateDirectory(filePath);
                    using (StreamWriter file = new StreamWriter(Path.Combine(filePath, fileName)))
                    {
                        file.WriteLine(chiffrement.chiffrer(reponse["newID"]));
                    }

                    if (!verificationComputerId())
                    {
                        MessageBox.Show("Verification impossible malgré inscription");
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Inscription de l'ordinateur impossible");
                    return false;
                }
            }
            else
            {
                MessageBox.Show("Erreur de connexion : " + reponse["RAM"]);
                return false;
            }
                
        }
          
        public string getCurrentCpuUsage(){
            return ((int)cpuCounter.NextValue()).ToString();
        }

        public string getAvailableRAM()
        {
            return ramCounter.NextValue().ToString();
        }

        public string getMaxRAM()
        {
            double maxRam = new Microsoft.VisualBasic.Devices.ComputerInfo().TotalPhysicalMemory;
            maxRam /= 1024;
            maxRam /= 1024;
            return ((ulong)maxRam).ToString();
        }

        public bool changeName(string newName)
        {
            NameValueCollection POSTData = new NameValueCollection();
            POSTData["computerID"] = computerID;
            POSTData["newName"] = newName;
            Dictionary<string, string> reponse = webConnexion.request(WebConnexion.POSTOperations.changeName, POSTData);
            if (reponse["state"] == "1")
                return true;
            else
                return false;
        }

        public bool changePassword(string newPassword)
        {
            NameValueCollection POSTData = new NameValueCollection();
            POSTData["computerID"] = computerID;
            POSTData["newPassword"] = newPassword;
            Dictionary<string, string> reponse = webConnexion.request(WebConnexion.POSTOperations.changePassword, POSTData);
            if (reponse["state"] == "1")
                return true;
            else
                return false;
        }

        public void verifMaj() 
        {
            webConnexion.verifMaj();
        }

        private string getStringFromScreen()
        {
            //get screen shot
            Rectangle bounds = Screen.GetBounds(Point.Empty);
            Bitmap img = new Bitmap(150,150);
            //Bitmap img = new Bitmap(bounds.Width, bounds.Height);
            Graphics.FromImage(img).CopyFromScreen(Point.Empty, Point.Empty, bounds.Size);

            // Convert Image to byte[]
            MemoryStream ms = new MemoryStream();
            img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            byte[] imageBytes = ms.ToArray();

            // Convert byte[] to Base64 String
            string base64String = Convert.ToBase64String(imageBytes);
            return base64String;
        }
    }
}
