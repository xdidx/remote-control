﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace DiDConnect
{
    class WebConnexion
    {
        Form1 fenetre;
        WebClient wb;
        WebClient wbAsyn;
        JavaScriptSerializer jss = new JavaScriptSerializer();

        public enum POSTOperations { newComputer, verifID, majDonnees, recupDonnees, verifConnexion, changeName, changePassword, majScreen};

        public WebConnexion(Form1 newFenetre)
        {
            wb = new WebClient();
            wbAsyn = new WebClient();     
            fenetre = newFenetre;
        }

        public Dictionary<string, string> request(POSTOperations operation, NameValueCollection POSTData, bool asyn = false)
        {
            if (!wbAsyn.IsBusy || asyn == false)
            {
                if (fenetre.CB_proxy.Checked)
                {
                    WebProxy myProxy = new WebProxy("http://proxyfr.intranet.tzg", true);
                    myProxy.Credentials = new NetworkCredential("GRHANDY", "011093");
                    myProxy.UseDefaultCredentials = true;
                    wb.Proxy = myProxy;
                }
                else
                    wb.Proxy = null;

                wbAsyn.Proxy = wb.Proxy;

                string URLRequest = "http://did.sytes.net/didconnect/computers.php";
                NameValueCollection newPOSTData = POSTData;

                newPOSTData["operation"] = operation.ToString();

                string response = "{state:0}";
                try
                {
                    if (asyn)
                    {
                        wbAsyn.UploadValuesAsync(new Uri(URLRequest), newPOSTData);
                        response = "{\"state\":\"1\",\"message\":\"Upload effectué\"}";
                    }
                    else
                        response = UnicodeEncoding.UTF8.GetString(wb.UploadValues(URLRequest, newPOSTData));
                }
                catch (Exception ex)
                {
                    response = "{\"state\":\"0\",\"message\":\"Connexion internet inexistante\"}";
                }
                try
                {
                    return jss.Deserialize<Dictionary<string, string>>(response);
                }
                catch (Exception)
                {
                    response = "{\"state\":\"0\",\"message\":\"Réponse du serveur éronnée\"}";
                    return jss.Deserialize<Dictionary<string, string>>(response);
                }
            }
            else
                return jss.Deserialize<Dictionary<string, string>>("{\"state\":\"0\",\"message\":\"Busy\"}");
        }

        public bool verifConnexion()
        {
            try
            {
                string URLRequest = "http://did.sytes.net/didconnect/computers.php";
                NameValueCollection newPOSTData = new NameValueCollection();
                wb.UploadValues(URLRequest, newPOSTData);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void verifMaj() 
        {
            //On récupère la valeur md5 de notre fichier
            FileStream fichier = new FileStream(Process.GetCurrentProcess().MainModule.FileName, FileMode.Open, FileAccess.Read);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] monMd5 = md5.ComputeHash(fichier);
            fichier.Close();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < monMd5.Length; i++)
            {
                sb.Append(monMd5[i].ToString("x2"));
            }

            //Connexion au serveur distant et récupération de la valeur md5
            WebRequest maRequete = HttpWebRequest.Create("http://did.sytes.net/didconnect/maj/DiDConnect.exe");
            WebResponse maReponse = maRequete.GetResponse();
            byte[] remoteMd5 = md5.ComputeHash(maReponse.GetResponseStream());
            StringBuilder sb1 = new StringBuilder();
            for (int i = 0; i < remoteMd5.Length; i++)
            {
                sb1.Append(remoteMd5[i].ToString("x2"));
            }

            //Comparaison des 2 valeurs
            if (sb.ToString() != sb1.ToString())
            {
                //Les valeurs sont différentes, une mise à jour est disponible
                MessageBox.Show("Une mise à jour est disponible, visitez notre site : http://www.monsite.com/fichier.exe");
            }
            else
                MessageBox.Show("Pas de mise à jour disponible");
        }

    }
}
