﻿using System.Text;
using System.Security.Cryptography;

namespace DiDConnect
{
    class Chiffrement
    {

        AesCryptoServiceProvider aesCSP;

        public Chiffrement()
        {
            aesCSP = new AesCryptoServiceProvider();

            aesCSP.Key = UnicodeEncoding.Unicode.GetBytes("did98765");
            aesCSP.IV = new byte[] { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 1, 2, 3, 4, 5, 6 };
            
        }

        public string chiffrer(string inString)
        {
            ICryptoTransform xfrm = aesCSP.CreateEncryptor();

            byte[] inBlock = UnicodeEncoding.Unicode.GetBytes(inString);
            byte[] outBlock = xfrm.TransformFinalBlock(inBlock, 0, inBlock.Length);
            //retour hexa
            //return BitConverter.ToString(outBlock);

            return UnicodeEncoding.Unicode.GetString(outBlock);
        }

        public string dechiffrer(string inString)
        {
            ICryptoTransform xfrm = aesCSP.CreateDecryptor();
            byte[] inBlock = UnicodeEncoding.Unicode.GetBytes(inString);
            /*
            int length=(inString.Length+1)/3;
            byte[] inBlock=new byte[length];
            for (int i = 0; i < length; i++)
            inBlock[i] = Convert.ToByte(inString.Substring(3 * i, 2), 16);
            */
            byte[] outBlock = xfrm.TransformFinalBlock(inBlock, 0, inBlock.Length);
            return UnicodeEncoding.Unicode.GetString(outBlock);

        }

    }
}
