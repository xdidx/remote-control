﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace DiDConnect
{
    class RemoteComputers
    {
        private System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        private string computerID;
        private Form1 fenetre;
        private WebConnexion webConnexion;

        public RemoteComputers(Form1 newFenetre)
        {
            fenetre = newFenetre;
            webConnexion = new WebConnexion(fenetre);
        }

        public void connexion(string newComputerID, string password)
        {
            if (computerID == newComputerID && timer.Enabled)
            {
                couperConnexion();
            }
            else
            {
                NameValueCollection POSTData = new NameValueCollection();
                computerID = newComputerID;
                POSTData["computerID"] = newComputerID;
                POSTData["password"] = password;

                Dictionary<string, string> reponse = webConnexion.request(WebConnexion.POSTOperations.verifConnexion, POSTData);
                if (reponse["state"] == "1")
                {
                    majDonnees(null, null);
                    fenetre.B_connexion.Text = "Terminer la connexion";
                    timer.Tick += new EventHandler(majDonnees);
                    timer.Interval = 500;//ms
                    timer.Start();
                }
                else if (reponse["state"] == "2")
                {
                    couperConnexion();
                    MessageBox.Show("L'authentification a échouée");
                }
                else
                {
                    couperConnexion();
                    MessageBox.Show("L'ordinateur n'est pas connecté");
                }
            }
        }
        
        private void majDonnees(object sender, EventArgs e)
        {
            NameValueCollection POSTData = new NameValueCollection();
            POSTData["computerID"] = computerID;
            Dictionary<string, string> reponse = webConnexion.request(WebConnexion.POSTOperations.recupDonnees, POSTData);
            if (reponse["state"] == "1")
            {
                if (reponse["name"] == "")
                    fenetre.L_remoteName.Text = "Ordinateur "+computerID;
                else
                    fenetre.L_remoteName.Text = reponse["name"];

                fenetre.L_remoteCPUUsage.Text = reponse["CPU"];
                fenetre.L_remoteRAMUsage.Text = reponse["RAM"];

                majImage();
       
            }
            else
            {
                couperConnexion();
            }
        }

        private void majImage()
        {
            NameValueCollection POSTData = new NameValueCollection();
            POSTData["computerID"] = computerID;
            Dictionary<string, string> reponse = webConnexion.request(WebConnexion.POSTOperations.recupDonnees, POSTData);
            if (reponse["state"] == "1")
            {
                try
                {
                    fenetre.PB_remoteScreen.Image = Base64ToImage(reponse["image"]);
                }
                catch(Exception)
                {

                }
            }
            else
            {
                MessageBox.Show("pb image : " + reponse["message"]);
            }
        }

        private void couperConnexion()
        {
            timer.Stop();
            computerID = "0";
            fenetre.L_remoteName.Text = "Connexion intérompue";
            fenetre.L_remoteCPUUsage.Text = " - ";
            fenetre.L_remoteRAMUsage.Text = " - ";
            fenetre.B_connexion.Text = "Connexion";
        }

        private Image Base64ToImage(string base64String)
        {

            // Convert Base64 String to byte[]
            byte[] imageBytes = new byte[0];
            for (int i = 0; i < (base64String.Length / 4); i++)            
                imageBytes = Combine(imageBytes, Convert.FromBase64String(base64String.Substring(i*4,4)));

            // Convert byte[] to Image
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true, true);
            return image;
            
        }

        private byte[] Combine(byte[] first, byte[] second)
        {
            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }
    }
}
