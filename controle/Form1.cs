﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using DiDConnect.Properties;
using System.Security.Cryptography;
using System.IO;

namespace DiDConnect
{
    public partial class Form1 : Form
    {

        private CurrentComputer currentComputer;
        private RemoteComputers ordinateur_distant;
        private Point mousePosition = new Point(0, 0);
        
        public Form1()
        {
            InitializeComponent();            

            currentComputer = new CurrentComputer(this);
            ordinateur_distant = new RemoteComputers(this);
            currentComputer.connexion();
            /*
            Thread InitialisationThread = new Thread(new ThreadStart(verifMaj));
            InitialisationThread.Start();   
             */       
        }

        private void verifMaj()
        {
            currentComputer.verifMaj();
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            mousePosition = new Point(e.X, e.Y);
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mousePosition.X != 0 && mousePosition.Y != 0)
            {
                this.Left += (e.X - mousePosition.X);
                this.Top += (e.Y - mousePosition.Y);
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            mousePosition = new Point(0, 0);
        }

        private void PB_reduct_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void PB_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PB_reduct_MouseEnter(object sender, EventArgs e)
        {
            Bitmap myImage = (Bitmap)Resources.ResourceManager.GetObject("reduire_hover");
            ((PictureBox)sender).Image = myImage;
        }

        private void PB_close_MouseEnter(object sender, EventArgs e)
        {
            Bitmap myImage = (Bitmap)Resources.ResourceManager.GetObject("fermer_hover");
            ((PictureBox)sender).Image = myImage;
        }

        private void PB_reduct_MouseLeave(object sender, EventArgs e)
        {
            Bitmap myImage = (Bitmap)Resources.ResourceManager.GetObject("reduire");
            ((PictureBox)sender).Image = myImage;
        }

        private void PB_close_MouseLeave(object sender, EventArgs e)
        {
            Bitmap myImage = (Bitmap)Resources.ResourceManager.GetObject("fermer");
            ((PictureBox)sender).Image = myImage;
        }

        private void B_connexion_Click(object sender, EventArgs e)
        {
            ordinateur_distant.connexion(TB_remoteID.Text, TB_remotePassword.Text);
        }

        private void B_changeName_Click(object sender, EventArgs e)
        {
            if (this.TB_pseudo.Text.Trim().Length < 3)
                MessageBox.Show("Le nom doit contenir au minimum 3 caractères");
            else
                if (currentComputer.changeName(TB_pseudo.Text.Trim()))
                    MessageBox.Show("Nom mis à jour");
                else
                    MessageBox.Show("Problème pendant la mise à jour du nom");
        }

        private void B_changePassword_Click(object sender, EventArgs e)
        {
            if (TB_password.Text != TB_passwordConfirm.Text)
                MessageBox.Show("Les mots de passes sont différents");
            else if (TB_password.Text.Length < 6)
                MessageBox.Show("Le mot de passe doit contenir au minimum 6 caractères");
            else
                if (currentComputer.changePassword(TB_password.Text))
                    MessageBox.Show("Mot de passe mis à jour");
                else
                    MessageBox.Show("Problème pendant la mise à jour du mot de passe");

        }
      
    }
  

}
